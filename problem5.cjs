const data = require('./data.cjs')
let problem4 = require('./problem4.cjs')


// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

function problem5(data) {
    let found;
    if (Array.isArray(data)) {
        if (data.length > 0) {
            found = data.filter(function (num) {
                return num > 2000;
            });
        }
    }
    return found.length
}

let year_data = problem4(data)
const result = problem5(year_data)
console.log(result)

module.exports = problem5
