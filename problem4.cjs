const data = require('./data.cjs')



// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.


function problem4(data) {

    if (Array.isArray(data)) {
        if (data.length > 0) {
            const car_year = data.reduce((accumulator, currentValue) => {
                accumulator.push(currentValue.car_year);
                return accumulator
              }, []);

            return car_year
        }
    }

}

const result = problem4(data)
console.log(result)

module.exports = problem4

