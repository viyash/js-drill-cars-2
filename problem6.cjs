const data = require('./data.cjs')


// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
function problem6(data) {
    let found = [];
    if (Array.isArray(data)) {
        if (data.length > 0) {
            found = data.filter(function (num) {
                return num.car_make == 'BMW' || num.car_make == 'Audi';
            });
        }
    }
    return JSON.stringify(found)
}

const result = problem6(data)
console.log(result)

module.exports = problem6
