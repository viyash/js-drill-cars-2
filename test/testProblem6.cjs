const problem = require('../problem6.cjs')
const assert = require('assert');


let data = [{"id":1,"car_make":"Infiniti","car_model":"G35","car_year":2004},
{"id":2,"car_make":"Lotus","car_model":"Esprit","car_year":2004},
{"id":3,"car_make":"Audi","car_model":"Cavalier","car_year":1997},
{"id":4,"car_make":"Dodge","car_model":"Ram Van 1500","car_year":1999},
{"id":5,"car_make":"BMW","car_model":"Intrepid","car_year":2000}]

const result = '[{"id":3,"car_make":"Audi","car_model":"Cavalier","car_year":1997},{"id":5,"car_make":"BMW","car_model":"Intrepid","car_year":2000}]'

assert.deepEqual(problem(data), result)